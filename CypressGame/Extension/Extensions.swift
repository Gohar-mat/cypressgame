//
//  Extensions.swift
//  CypressGame
//
//  Created by Gohar on 12/21/18.
//  Copyright © 2018 Gohar. All rights reserved.
//

import Foundation
import UIKit

//MARK: raunded Views

extension UIView {
    func setCornerRadius(button: UIButton)  {
        layer.cornerRadius = 5
        layer.masksToBounds = true
    }
}

extension ViewController {
    func displayMessage(title: String, userMessage:String) {
        let alert = UIAlertController(title: title, message:userMessage, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (alert) in
            self.dismiss(animated: true, completion: nil)
            self.newGame()
        }))
        self.present(alert, animated: true, completion: nil)
    }
}
