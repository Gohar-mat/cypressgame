//
//  RandomIcons.swift
//  CypressGame
//
//  Created by Gohar on 12/25/18.
//  Copyright © 2018 Gohar. All rights reserved.
//

import Foundation

class RandomIcons {
    
    enum RandomIcons: UInt {
        case animal
        case plants
        case weather
        case smile
        case person
    }
    
    /// get an array of icons by RandomIcons
    func getIcons(by theme: RandomIcons) -> [String] {
        switch theme {
        case .animal:
            return ["🐶", "🐝", "🐷", "🦊", "🦉", "🐭", "🐥", "🐼"]
        case .plants:
            return ["🍁", "🍄", "🌹", "💐", "🌾", "🌸", "🌺", "🌻"]
        case .weather:
            return ["🌝", "❄️", "☀️", "💦", "☔️", "🌬", "💫", "🌪", "⛈"]
        case .smile:
            return ["🤔", "😇", "🥳", "🤨", "🤓", "😡", "🥺", "🧐"]
        case .person:
            return ["👩🏻‍💻", "👩🏻‍🚀", "🧒🏻", "👫", "🎅", "🤶", "🤴", "🤵", "👩‍🎨"]
        }
    }
    
    private func random() -> RandomIcons {
        let lastValue = RandomIcons.person.rawValue
        let randomIndex = arc4random_uniform(UInt32(lastValue) + UInt32(1))
        return RandomIcons(rawValue: UInt(randomIndex)) ?? RandomIcons.animal
    }
    
    func getRandomIcons() -> [String] {
        return getIcons(by: random())
    }
    
    
    
}
