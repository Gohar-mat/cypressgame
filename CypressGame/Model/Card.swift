//
//  Card.swift
//  CypressGame
//
//  Created by Gohar on 12/19/18.
//  Copyright © 2018 Gohar. All rights reserved.
//

import Foundation
import UIKit

struct Card  {
    
    var tag: Int
    var id: Int
    var isMatched = false
    var isFlipped = false
    
    static var identifierFactory = 0

    static func getUniqueIdentifier() -> Int {
        identifierFactory += 1
        return identifierFactory
    }
    
    static var tagFactory = 0
    
    static func getUniqueTag() -> Int {
        tagFactory += 1
        return tagFactory
    }
    
    init(id: Int, tag: Int) {
        self.id = id
        self.tag = tag
    }
    
}

