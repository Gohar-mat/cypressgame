//
//  ViewController.swift
//  CypressGame
//
//  Created by Gohar on 12/14/18.
//  Copyright © 2018 Gohar. All rights reserved.
//

import UIKit

class ViewController: UIViewController, WinEvent {
    
    //MARK: IBOutlet
    @IBOutlet var emojis: [UIButton]!
    @IBOutlet weak var counter: UILabel!
    
    //MARK: Constants
    let ANIMATION_DURATION = 0.3
    
    //MARK: Variables
    
    lazy var game = CompareCarts(numberOfPairsOfCards: emojis.count/2)
    private var randomIcons = RandomIcons()
    private lazy var emojiChoices = randomIcons.getRandomIcons()
    private var emoji = [Int:String]()
    
    //MARK: LifeCicle Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for (index,card) in game.cards.enumerated() {
            emojis[index].tag = card.tag
        }
        game.delegate = self
        emojis.shuffle()        // make random emojis
        setCorner()
    }
    
    //MARK: Set corener radius to buttons
    func setCorner() {
        for emoji in emojis {
            emoji.setCornerRadius(button: emoji)
        }
    }
    
    //MARK: Create random Emojis
    func emoji(for card: Card) -> String {
        if emoji[card.id] == nil, emojiChoices.count > 0 {
            let randomIndex = Int(arc4random_uniform(UInt32(emojiChoices.count)))
            emoji[card.id] = emojiChoices.remove(at: randomIndex)
        }
        return emoji[card.id] ?? "?"
    }
    
    func updateViewFromModel(tag: Int = -1) {
        for (index, button) in emojis.enumerated() {
            let card = game.cards[index]
            counter.text = "Attempts: \(game.flipCount)"
            
            if card.isFlipped {
                button.setTitle(self.emoji(for: card), for: UIControl.State.normal)
                button.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            } else {
                button.setTitle("", for: UIControl.State.normal)
                button.backgroundColor = card.isMatched ? #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0) : #colorLiteral(red: 0.01258557719, green: 0.4059261658, blue: 0.03405468331, alpha: 1)
            }
            
            if  button.tag == tag && card.isFlipped && !game.tagOfOneAndOnlyFaseUpCard  {
                UIView.transition(with: button,
                                  duration: ANIMATION_DURATION,
                                  options: .transitionFlipFromRight,
                                  animations: nil, completion: nil) // check if true
            }
            
            if card.isMatched {
                UIView.transition(with: button,
                                  duration: ANIMATION_DURATION,
                                  options: .transitionFlipFromRight,
                                  animations: nil) { (matched) in
                                    button.bounds.size.height = 0
                                    button.bounds.size.width = 0
                }
            }
        }
    }
    
    func win() {
        displayMessage(title: "Congratulations!", userMessage: "You've finished the game !🥳 🍭 🤗 You've \(game.flipCount) attempts")
    }
    
    //MARK: IBAction
    @IBAction func touchOnEmoji(_ sender: UIButton) {
        if let cardNumber = emojis.index(of: sender) {
           
            game.chooseCard(at: cardNumber)
            updateViewFromModel(tag: sender.tag)
        }
    }
    
    @IBAction func resetGame(_ sender: UIButton) {
        newGame()
    }
    
    func newGame() {
        //reset game
        game = CompareCarts.init(numberOfPairsOfCards: emojis.count)
        
        //update View
        for emoji in emojis {
            updateViewFromModel(tag: emoji.tag)
        }
        
        //reset Icon choices
        emojiChoices = randomIcons.getRandomIcons()
    }
    
}


