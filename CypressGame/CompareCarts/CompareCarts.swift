//
//  CompareCarts.swift
//  CypressGame
//
//  Created by Gohar on 12/25/18.
//  Copyright © 2018 Gohar. All rights reserved.
//

import Foundation

class CompareCarts {
    
    var delegate: WinEvent?
    
    let numberOfPairs: Int
    
    var flipCount: Int = 0
    var matchedCount: Int = 0 {
        didSet {
            if matchedCount == numberOfPairs {
                if let delegate = delegate {
                    delegate.win()
                }
                matchedCount = 0
            }
        }
    }
    
    var cards = [Card]()
    var indexOfOneAndOnlyFaseUpCard: Int?
    var tagOfOneAndOnlyFaseUpCard: Bool = false
    

    func chooseCard(at index: Int) {
        if !cards[index].isMatched {
            if let matchIndex = indexOfOneAndOnlyFaseUpCard, matchIndex != index {
                tagOfOneAndOnlyFaseUpCard = false
                
                // 2 cards are face up, check if cards match
                if cards[matchIndex].id == cards[index].id {
                    matchedCount += 1
                    cards[matchIndex].isMatched = true
                    cards[index].isMatched = true
                } else {
                    flipCount += 1
            }
                
                cards[index].isFlipped = true
                indexOfOneAndOnlyFaseUpCard = nil
                
            } else if let matchIndex = indexOfOneAndOnlyFaseUpCard, index == matchIndex {
                tagOfOneAndOnlyFaseUpCard = true
                
            } else {
                tagOfOneAndOnlyFaseUpCard = false
                for flipDownIndex in cards.indices {
                    cards[flipDownIndex].isFlipped = flipDownIndex == index
                }
                indexOfOneAndOnlyFaseUpCard = index
            }
        }
    }
    
    init(numberOfPairsOfCards: Int) {
        numberOfPairs = numberOfPairsOfCards
        for _ in 1...numberOfPairsOfCards {
            let id = Card.getUniqueIdentifier()
            let card1 = Card(id: id, tag: Card.getUniqueTag())
            let card2 = Card(id: id, tag: Card.getUniqueTag())
            cards += [card1,card2]
        }
    }
}

protocol WinEvent {
    func win()
}
